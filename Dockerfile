ARG BASE_IMAGE="alpine:3.15"
FROM $BASE_IMAGE

ARG BUILD_DATE
ARG GIT_REV
ARG DOCKER_TAG="0.1"

LABEL \
    org.opencontainers.image.title="alpine-mariadb" \
    org.opencontainers.image.description="MariaDB Docker image running on Alpine Linux" \
    org.opencontainers.image.authors="GP Orcullo<kinsamanka@gmail.com>" \
    org.opencontainers.image.version=$DOCKER_TAG \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/kinsamanka/alpine-mariadb" \
    org.opencontainers.image.source="https://gitlab.com/kinsa13/tango/Docker/alpine-mariadb" \
    org.opencontainers.image.revision=$GIT_REV \
    org.opencontainers.image.created=$BUILD_DATE

RUN apk add --no-cache mariadb mariadb-client mariadb-server-utils pwgen && \
    rm -f /var/cache/apk/* && \
    mkdir /docker-entrypoint-initdb.d && \
    mkdir -p /scripts/pre-exec.d && \
    mkdir /scripts/pre-init.d && \
    chmod -R 755 /scripts

ADD run.sh /scripts/

ENTRYPOINT ["/scripts/run.sh"]

