# MariaDB Docker image running on Alpine Linux

This Dockerfile is based on the work done here: https://github.com/yobasystems/alpine-mariadb

```
docker build --build-arg GIT_REV="$(git rev-parse HEAD)" --build-arg BUILD_DATE="$(date)" -t <tag>
```
